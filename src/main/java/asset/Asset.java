package asset;


public class Asset {
	private String name;
	private String symbol;
	
	public Asset(String name, String symbol) {
		this.name = name;
		this.symbol = symbol;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSymbol() {
		return this.symbol;
	}
}
