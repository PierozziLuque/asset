package assetTest;

import org.junit.Before;
import org.junit.Test;

import asset.Asset;

import static org.junit.Assert.*;


public class AssetTest {
	Asset asset;

	@Before
	public void init() {
		asset = new Asset("BITCOIN", "BTC");
	}
	
	@Test
	public void testAssetGetSymbolOk() {
		assertTrue(asset.getSymbol().equals("BTC"));
	}
	
	@Test
	public void testAssetGetNamelOk() {
		assertTrue(asset.getName().equals("BITCOIN"));
	}
}
